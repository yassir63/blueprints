#!/bin/bash

# This script installs various tools and dependencies for BPF development.

# Ensure the script is run as root
if [ "$EUID" -ne 0 ]; then 
  echo "Please run as root"
  exit
fi

# Update the package list
apt update

# Install the required packages
apt install -y bpfcc-tools libmicrohttpd-dev linux-headers-$(uname -r) docker.io python3-pip git iperf3 clang llvm libelf-dev libpcap-dev gcc-multilib build-essential make linux-tools-generic linux-tools-common

# Clone and install libbpf
cd ~
git clone --depth 1 https://github.com/libbpf/libbpf
cd libbpf/src
sudo make install
cd ~

# Remove any existing bpftool
rm -f /usr/sbin/bpftool

# Clone, build, and install bpftool
cd /
git clone --recurse-submodules https://github.com/libbpf/bpftool.git
cd bpftool/src
make install

# Create a symbolic link for bpftool
ln -s /usr/local/sbin/bpftool /usr/sbin/bpftool

cd ~/ebpf
bpftool btf dump file /sys/kernel/btf/vmlinux format c > vmlinux.h

pip install pandas prometheus_client

#turn-on-data

echo "Installation complete. All tools are installed and ready to use."
