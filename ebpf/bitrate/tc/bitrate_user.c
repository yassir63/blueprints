// // // #include <stdio.h>
// // // #include <unistd.h>
// // // #include <fcntl.h>
// // // #include <errno.h>
// // // #include <linux/bpf.h>
// // // #include <bpf/libbpf.h>
// // // #include <bpf/bpf.h>

// // // #define MAP_PATH "/sys/fs/bpf/tc/globals/packet_stats"

// // // struct datarec {
// // //     __u64 total_length;
// // //     __u64 last_update;
// // // };

// // // int main() {
// // //     int map_fd;
// // //     struct datarec rec;
// // //     __u32 key = 0;

// // //     map_fd = bpf_obj_get(MAP_PATH);
// // //     if (map_fd < 0) {
// // //         fprintf(stderr, "Failed to open bpf map: %s\n", strerror(errno));
// // //         return 1;
// // //     }

// // //     while (1) {
// // //         sleep(1);
// // //         if (bpf_map_lookup_elem(map_fd, &key, &rec) != 0) {
// // //             fprintf(stderr, "Failed to lookup bpf map: %s\n", strerror(errno));
// // //             close(map_fd);
// // //             return 1;
// // //         }
        
// // //         // Convert bytes to megabits
// // //         __u64 total_length_bits = rec.total_length * 8;
// // //         double total_length_mbits = total_length_bits / 1000000.0;
// // //         printf("Total packet length in last second: %.2f Mbits\n", total_length_mbits);

// // //         // Reset the map entry
// // //         rec.total_length = 0;
// // //         rec.last_update = 0;
// // //         if (bpf_map_update_elem(map_fd, &key, &rec, BPF_ANY) != 0) {
// // //             fprintf(stderr, "Failed to reset map entry: %s\n", strerror(errno));
// // //             close(map_fd);
// // //             return 1;
// // //         }
// // //     }

// // //     close(map_fd);
// // //     return 0;
// // // }


// // #include <stdio.h>
// // #include <unistd.h>
// // #include <fcntl.h>
// // #include <errno.h>
// // #include <linux/bpf.h>
// // #include <bpf/libbpf.h>
// // #include <bpf/bpf.h>

// // #define MAP_PATH "/sys/fs/bpf/tc/globals/packet_stats"
// // #define LOG_FILE "bitrate.txt"

// // struct datarec {
// //     __u64 total_length;
// //     __u64 last_update;
// // };

// // int main() {
// //     int map_fd;
// //     struct datarec rec;
// //     __u32 key = 0;
// //     FILE *log_file;

// //     map_fd = bpf_obj_get(MAP_PATH);
// //     if (map_fd < 0) {
// //         fprintf(stderr, "Failed to open bpf map: %s\n", strerror(errno));
// //         return 1;
// //     }

// //     log_file = fopen(LOG_FILE, "a");
// //     if (log_file == NULL) {
// //         fprintf(stderr, "Failed to open log file: %s\n", strerror(errno));
// //         close(map_fd);
// //         return 1;
// //     }

// //     while (1) {
// //         sleep(1);
// //         if (bpf_map_lookup_elem(map_fd, &key, &rec) != 0) {
// //             fprintf(stderr, "Failed to lookup bpf map: %s\n", strerror(errno));
// //             close(map_fd);
// //             fclose(log_file);
// //             return 1;
// //         }
        
// //         // Convert bytes to megabits
// //         __u64 total_length_bits = rec.total_length * 8;
// //         double total_length_mbits = total_length_bits / 1000000.0;
// //         printf("Total packet length in last second: %.2f Mbits\n", total_length_mbits);

// //         // Log the value to the file
// //         fprintf(log_file, "Key: %u, Total Length: %llu, Last Update: %llu, Total Length in Mbits: %.2f\n", key, rec.total_length, rec.last_update, total_length_mbits);
// //         fflush(log_file); // Ensure the data is written to the file

// //         // Reset the map entry
// //         rec.total_length = 0;
// //         rec.last_update = 0;
// //         if (bpf_map_update_elem(map_fd, &key, &rec, BPF_ANY) != 0) {
// //             fprintf(stderr, "Failed to reset map entry: %s\n", strerror(errno));
// //             close(map_fd);
// //             fclose(log_file);
// //             return 1;
// //         }
// //     }

// //     close(map_fd);
// //     fclose(log_file);
// //     return 0;
// // }



// #include <stdio.h>
// #include <unistd.h>
// #include <fcntl.h>
// #include <errno.h>
// #include <linux/bpf.h>
// #include <bpf/libbpf.h>
// #include <bpf/bpf.h>

// #define MAP_PATH "/sys/fs/bpf/tc/globals/packet_stats"
// #define LOG_FILE "bitrate.txt"

// struct datarec {
//     __u64 total_length;
//     __u64 last_update;
//     __u64 packet_count; // New field for packet count
// };

// int main() {
//     int map_fd;
//     struct datarec rec;
//     __u32 key = 0;
//     FILE *log_file;

//     map_fd = bpf_obj_get(MAP_PATH);
//     if (map_fd < 0) {
//         fprintf(stderr, "Failed to open bpf map: %s\n", strerror(errno));
//         return 1;
//     }

//     log_file = fopen(LOG_FILE, "a");
//     if (log_file == NULL) {
//         fprintf(stderr, "Failed to open log file: %s\n", strerror(errno));
//         close(map_fd);
//         return 1;
//     }

//     while (1) {
//         sleep(1);
//         if (bpf_map_lookup_elem(map_fd, &key, &rec) != 0) {
//             fprintf(stderr, "Failed to lookup bpf map: %s\n", strerror(errno));
//             close(map_fd);
//             fclose(log_file);
//             return 1;
//         }
        
//         // Convert bytes to megabits
//         __u64 total_length_bits = rec.total_length * 8;
//         double total_length_mbits = total_length_bits / 1000000.0;
//         printf("Total packet length in last second: %.2f Mbits\n", total_length_mbits);
//         printf("Total packet count: %llu packets\n", rec.packet_count); // Cumulative packet count

//         // Log the value to the file
//         fprintf(log_file, "Key: %u, Total Length: %llu, Last Update: %llu, Total Length in Mbits: %.2f, Packet Count: %llu\n",
//                 key, rec.total_length, rec.last_update, total_length_mbits, rec.packet_count);
//         fflush(log_file); // Ensure the data is written to the file

//         // Reset the total length but not the packet count
//         rec.total_length = 0;
//         rec.last_update = 0;
//         if (bpf_map_update_elem(map_fd, &key, &rec, BPF_ANY) != 0) {
//             fprintf(stderr, "Failed to reset map entry: %s\n", strerror(errno));
//             close(map_fd);
//             fclose(log_file);
//             return 1;
//         }
//     }

//     close(map_fd);
//     fclose(log_file);
//     return 0;
// }


#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <linux/bpf.h>
#include <bpf/libbpf.h>
#include <bpf/bpf.h>
#include <microhttpd.h>

#define MAP_PATH "/sys/fs/bpf/tc/globals/packet_stats"
#define HTTP_PORT 9268

struct datarec {
    __u64 total_length;
    __u64 last_update;
    __u64 packet_count;
};

static double total_length_mbits = 0;
static __u64 packet_count = 0;

int request_handler(void *cls, struct MHD_Connection *connection, const char *url,
                    const char *method, const char *version, const char *upload_data,
                    size_t *upload_data_size, void **con_cls) {
    if (strcmp(method, "GET") != 0) {
        return MHD_NO;
    }

    char metrics[256];
    snprintf(metrics, sizeof(metrics),
             "# HELP total_length_mbits Total packet length in Mbits\n"
             "# TYPE total_length_mbits gauge\n"
             "total_length_mbits %.2f\n"
             "# HELP packet_count Total packet count\n"
             "# TYPE packet_count gauge\n"
             "packet_count %llu\n",
             total_length_mbits, packet_count);

    struct MHD_Response *response = MHD_create_response_from_buffer(strlen(metrics), (void *)metrics, MHD_RESPMEM_MUST_COPY);
    int ret = MHD_queue_response(connection, MHD_HTTP_OK, response);
    MHD_destroy_response(response);

    return ret;
}

void start_http_server(int port) {
    struct MHD_Daemon *daemon;
    daemon = MHD_start_daemon(MHD_USE_SELECT_INTERNALLY, port, NULL, NULL, &request_handler, NULL, MHD_OPTION_END);
    if (daemon == NULL) {
        fprintf(stderr, "Failed to start HTTP server\n");
        exit(1);
    }
}

int main() {
    int map_fd;
    struct datarec rec;
    __u32 key = 0;

    // Start HTTP server
    start_http_server(HTTP_PORT);

    map_fd = bpf_obj_get(MAP_PATH);
    if (map_fd < 0) {
        fprintf(stderr, "Failed to open bpf map: %s\n", strerror(errno));
        return 1;
    }

    while (1) {
        sleep(1);
        if (bpf_map_lookup_elem(map_fd, &key, &rec) != 0) {
            fprintf(stderr, "Failed to lookup bpf map: %s\n", strerror(errno));
            close(map_fd);
            return 1;
        }

        // Convert bytes to megabits
        __u64 total_length_bits = rec.total_length * 8;
        total_length_mbits = total_length_bits / 1000000.0;
        packet_count = rec.packet_count;

        printf("Total packet length in last second: %.2f Mbits\n", total_length_mbits);
        printf("Total packet count: %llu packets\n", packet_count);

        // Reset the total length but not the packet count
        rec.total_length = 0;
        rec.last_update = 0;
        if (bpf_map_update_elem(map_fd, &key, &rec, BPF_ANY) != 0) {
            fprintf(stderr, "Failed to reset map entry: %s\n", strerror(errno));
            close(map_fd);
            return 1;
        }
    }

    close(map_fd);
    return 0;
}

