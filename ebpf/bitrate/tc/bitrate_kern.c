// #include <linux/bpf.h>
// #include <linux/if_ether.h>
// #include <linux/ip.h>
// #include <linux/in.h>
// #include <linux/tcp.h>
// #include <linux/udp.h>
// #include <bpf/bpf_helpers.h>
// #include <bpf/bpf_endian.h>

// struct datarec {
//     __u64 total_length;
//     __u64 last_update;
// };

// struct {
//     __uint(type, BPF_MAP_TYPE_ARRAY);
//     __type(key, __u32);
//     __type(value, struct datarec);
//     __uint(pinning, LIBBPF_PIN_BY_NAME);
//     __uint(max_entries, 1);
// } packet_stats SEC(".maps");

// SEC("socket_filter")
// int packet_length_counter(struct __sk_buff *skb) {
//     __u16 eth_proto;
//     __u64 eth_proto_offset = offsetof(struct ethhdr, h_proto);
//     if (bpf_skb_load_bytes(skb, eth_proto_offset, &eth_proto, sizeof(eth_proto)) < 0) {
//         bpf_printk("Failed to load Ethernet protocol type");
//         return 0;
//     }

//     eth_proto = bpf_ntohs(eth_proto);
//     if (eth_proto != ETH_P_IP) {
//         bpf_printk("Not an IP packet");
//         return 0;
//     }

//     __u32 key = 0;
//     struct datarec *rec = bpf_map_lookup_elem(&packet_stats, &key);
//     if (!rec) {
//         bpf_printk("Failed to get datarec");
//         return 0;
//     }

//     __u64 current_time = bpf_ktime_get_ns();
//     __u64 time_diff = current_time - rec->last_update;

//     if (time_diff >= 1000000000) { // 1 second in nanoseconds
//         rec->total_length = 0;
//         rec->last_update = current_time;
//     }

//     __sync_fetch_and_add(&rec->total_length, skb->len);
//     bpf_printk("Packet length added: %u, Total length: %llu", skb->len, rec->total_length);

//     return 0;
// }

// char _license[] SEC("license") = "GPL";


#include <linux/bpf.h>
#include <linux/if_ether.h>
#include <linux/ip.h>
#include <linux/in.h>
#include <linux/tcp.h>
#include <linux/udp.h>
#include <bpf/bpf_helpers.h>
#include <bpf/bpf_endian.h>

struct datarec {
    __u64 total_length;
    __u64 last_update;
    __u64 packet_count; // New field for packet count
};

struct {
    __uint(type, BPF_MAP_TYPE_ARRAY);
    __type(key, __u32);
    __type(value, struct datarec);
    __uint(pinning, LIBBPF_PIN_BY_NAME);
    __uint(max_entries, 1);
} packet_stats SEC(".maps");

SEC("socket_filter")
int packet_length_counter(struct __sk_buff *skb) {
    __u16 eth_proto;
    __u64 eth_proto_offset = offsetof(struct ethhdr, h_proto);
    if (bpf_skb_load_bytes(skb, eth_proto_offset, &eth_proto, sizeof(eth_proto)) < 0) {
        bpf_printk("Failed to load Ethernet protocol type");
        return 0;
    }

    eth_proto = bpf_ntohs(eth_proto);
    if (eth_proto != ETH_P_IP) {
        bpf_printk("Not an IP packet");
        return 0;
    }

    __u32 key = 0;
    struct datarec *rec = bpf_map_lookup_elem(&packet_stats, &key);
    if (!rec) {
        bpf_printk("Failed to get datarec");
        return 0;
    }

    __u64 current_time = bpf_ktime_get_ns();
    __u64 time_diff = current_time - rec->last_update;

    if (time_diff >= 1000000000) { // 1 second in nanoseconds
        rec->total_length = 0;
        rec->last_update = current_time;
    }

    __sync_fetch_and_add(&rec->total_length, skb->len);
    __sync_fetch_and_add(&rec->packet_count, 1); // Increment packet count
    bpf_printk("Packet length added: %u, Total length: %llu, Packet count: %llu", skb->len, rec->total_length, rec->packet_count);

    return 0;
}

char _license[] SEC("license") = "GPL";

