# import os
# import time
# import subprocess
# import pandas as pd
# from datetime import datetime
# from prometheus_client import start_http_server, Gauge

# # Define Prometheus metrics
# average_delay_gauge = Gauge('average_delay', 'Average delay calculated by the script')

# commands = [
#     "chmod +x ../latency1/tcplatency1_user",
#     "chmod +x ../latency4/tcplatency4_user",
#     "../latency1/./tcplatency1_user",
#     "../latency4/./tcplatency4_user"
# ]

# def read_output(file_name):
#     with open(file_name, 'r') as file:
#         lines = file.readlines()
#     data = {}
#     for line in lines:
#         parts = line.strip().split(",")
#         key = int(parts[0].split(": ")[1])
#         # sequence = int(parts[4].split(": ")[1])
#         time_val = int(parts[1].split(": ")[1])
#         if key not in data:
#             data[key] = []
#         data[key].append((key, time_val))
#     return data


# def find_closest_bigger_timestamp(output, target_time):
#     closest_entry = None
#     closest_time = float('inf')
#     for key, entries in output.items():
#         for _,time_val in entries:
#             if time_val > target_time and time_val < closest_time:
#                 closest_time = time_val
#                 closest_entry = (key, time_val)
#     return closest_entry

# def calculate_delays(output1, output2, output3, output4):
#     delays = []
#     for key1, entries1 in output1.items():
#         for _,time1 in entries1:
#             if key1 in output2:
#                 for _,time2 in output2[key1]:
#                     closest_entry3 = find_closest_bigger_timestamp(output3, time2)
#                     if closest_entry3:
#                         key3, time3 = closest_entry3
#                         if key3 in output4:
#                             for _,time4 in output4[key3]:
#                                 # if sequence4 == sequence3:
#                                     final_delay = time4 - time1 - time3 + time2
#                                     delays.append((time1, time2, time3, time4, final_delay))
#     return delays

# def main():
#     # Start Prometheus metrics server
#     start_http_server(9267)
#     print("Prometheus metrics server started on port 9267")

#     while True:
#         output_files = ['output2.txt', 'output3.txt']

#         all_files_exist = all(os.path.exists(file) for file in output_files)
#         if all_files_exist:

#             for command in commands:
#                 try:
#                     result = subprocess.run(command, shell=True, check=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
#                     print(f"Command '{command}' executed successfully.")
#                 except subprocess.CalledProcessError as e:
#                     print(f"Error executing command '{command}': {e.stderr.decode('utf-8')}")


#             output1 = read_output('output1.txt')
#             output2 = read_output('output2.txt')
#             output3 = read_output('output3.txt')
#             output4 = read_output('output4.txt')

#             delays = calculate_delays(output1, output2, output3, output4)
#             if delays:
#                 total_delay = 0
#                 for time1, time2, time3, time4, final_delay in delays:
#                     print(f"Times: {time1}, {time2}, {time3}, {time4} -> Final delay: {final_delay}")
#                     if final_delay > 0:
#                         total_delay += final_delay
#                 average_delay = total_delay / len(delays)
#                 print(f"Average delay: {average_delay}")

#                 # Update Prometheus metric
#                 average_delay_gauge.set(average_delay)
#             else:
#                 print("No matches found.")

#             # Delete the output files
#             for file in output_files:
#                 os.remove(file)
            
#             # Wait 5 seconds before the next iteration
#             time.sleep(5)
#         else:
#             print("Waiting for all output files to be available...")
#             time.sleep(5)

# if __name__ == "__main__":
#     main()


import os
import time
import subprocess
import pandas as pd
from datetime import datetime
from prometheus_client import start_http_server, Gauge

# Define Prometheus metrics
average_delay_gauge = Gauge('average_delay', 'Average delay calculated by the script')
ping_delay_gauge = Gauge('ping_delay', 'Ping delay to ')

commands = [
    "chmod +x ../latency1/tcplatency1_user",
    "chmod +x ../latency4/tcplatency4_user",
    "../latency1/./tcplatency1_user",
    "../latency4/./tcplatency4_user"
]

def read_output(file_name):
    with open(file_name, 'r') as file:
        lines = file.readlines()
    data = {}
    for line in lines:
        parts = line.strip().split(",")
        key = int(parts[0].split(": ")[1])
        time_val = int(parts[1].split(": ")[1])
        if key not in data:
            data[key] = []
        data[key].append((key, time_val))
    return data

def find_closest_bigger_timestamp(output, target_time):
    closest_entry = None
    closest_time = float('inf')
    for key, entries in output.items():
        for _, time_val in entries:
            if time_val > target_time and time_val < closest_time:
                closest_time = time_val
                closest_entry = (key, time_val)
    return closest_entry

def calculate_delays(output1, output2, output3, output4):
    delays = []
    for key1, entries1 in output1.items():
        for _, time1 in entries1:
            if key1 in output2:
                for _, time2 in output2[key1]:
                    closest_entry3 = find_closest_bigger_timestamp(output3, time2)
                    if closest_entry3:
                        key3, time3 = closest_entry3
                        if key3 in output4:
                            for _, time4 in output4[key3]:
                                final_delay = time4 - time1 - time3 + time2
                                delays.append((time1, time2, time3, time4, final_delay))
    return delays   

def run_ping():
    try:
        result = subprocess.run(['ping', '-c', '1', '192.168.3.1'], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        output = result.stdout.decode('utf-8')
        for line in output.split('\n'):
            if 'time=' in line:
                ping_time = float(line.split('time=')[1].split(' ')[0])
                return ping_time
    except subprocess.CalledProcessError as e:
        print(f"Error executing ping command: {e.stderr.decode('utf-8')}")
    return None

def main():
    # Start Prometheus metrics server
    start_http_server(9267)
    print("Prometheus metrics server started on port 9267")

    while True:
        output_files = ['output2.txt', 'output3.txt']

        all_files_exist = all(os.path.exists(file) for file in output_files)
        if all_files_exist:

            for command in commands:
                try:
                    result = subprocess.run(command, shell=True, check=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                    print(f"Command '{command}' executed successfully.")
                except subprocess.CalledProcessError as e:
                    print(f"Error executing command '{command}': {e.stderr.decode('utf-8')}")


            output1 = read_output('output1.txt')
            output2 = read_output('output2.txt')
            output3 = read_output('output3.txt')
            output4 = read_output('output4.txt')

            delays = calculate_delays(output1, output2, output3, output4)
            if delays:
                total_delay = 0
                for time1, time2, time3, time4, final_delay in delays:
                    print(f"Times: {time1}, {time2}, {time3}, {time4} -> Final delay: {final_delay}")
                    if final_delay > 0:
                        total_delay += final_delay
                average_delay = total_delay / len(delays)
                print(f"Average delay: {average_delay}")

                # Update Prometheus metric
                average_delay_gauge.set(average_delay)
            else:
                print("No matches found.")

            # Run ping command
            ping_delay = run_ping()
            if ping_delay:
                print(f"Ping delay: {ping_delay} ms")
                ping_delay_gauge.set(ping_delay)

            # Delete the output files
            for file in output_files:
                os.remove(file)
            
            # Wait 5 seconds before the next iteration
            time.sleep(5)
        else:
            print("Waiting for all output files to be available...")
            time.sleep(5)

if __name__ == "__main__":
    main()
