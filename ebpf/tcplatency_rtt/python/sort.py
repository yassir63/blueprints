import re

def parse_line(line):
    pattern = (r"Hash Key: (\d+), Source IP: ([\d.]+), Destination IP: ([\d.]+), "
               r"Protocol: (\d+), Sequence: (\d+), Length: (\d+), Time: (\d+)")
    match = re.match(pattern, line)
    if match:
        return {
            "hash_key": int(match.group(1)),
            "source_ip": match.group(2),
            "destination_ip": match.group(3),
            "protocol": int(match.group(4)),
            "sequence": int(match.group(5)),
            "length": int(match.group(6)),
            "time": int(match.group(7)),
            "original_line": line
        }
    return None

def sort_and_sum_lengths(input_file):
    with open(input_file, 'r') as file:
        lines = file.readlines()

    parsed_lines = [parse_line(line) for line in lines if parse_line(line) is not None]
    sorted_lines = sorted(parsed_lines, key=lambda x: x["time"])

    initial_timestamp = sorted_lines[0]["time"]
    target_timestamp = initial_timestamp + 1_000_000_000  # 1 second later

    print(initial_timestamp)
    print(target_timestamp)

    total_length = 0
    for entry in sorted_lines:
        if initial_timestamp <= entry["time"] <= target_timestamp:
            total_length += entry["length"]

    return total_length * 8 / 1000000

# Usage
input_file = 'output3.txt'
total_length = sort_and_sum_lengths(input_file)
print(f"Total length of packets in the 1-second interval: {total_length} Mbps")
