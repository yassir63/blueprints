#include <linux/bpf.h>
#include <linux/if_ether.h>
#include <linux/ip.h>
#include <linux/in.h>
#include <linux/tcp.h>
#include <linux/udp.h>
#include <bpf/bpf_helpers.h>
#include <bpf/bpf_endian.h>

// struct packet_info {
//     __u32 src_ip;
//     __u32 dst_ip;
//     // __u16 src_port;
//     // __u16 dst_port;
//     __u16 protocol;
//     __u32 sequence;  // For TCP
//     __u32 length;
//     __u32 payload_hash;
//     __u64 time;
// } __attribute__((packed));

struct packet_info {
    __u64 time;
} __attribute__((packed));

struct {
    __uint(type, BPF_MAP_TYPE_HASH);
    __type(key, __u32);
    __type(value, struct packet_info);
    __uint(pinning, LIBBPF_PIN_BY_NAME);
    __uint(max_entries, 50000);
} tcplatency4 SEC(".maps");

static __always_inline __u32 hash_key(__u32 src_ip, __u32 dst_ip, __u8 protocol, __u32 payload_hash) {
    __u32 hash = 0;
    hash += src_ip;
    hash += (hash << 10);
    hash ^= (hash >> 6);
    hash += dst_ip;
    hash += (hash << 10);
    hash ^= (hash >> 6);
    hash += protocol;
    hash += (hash << 10);
    hash ^= (hash >> 6);
    hash += payload_hash;
    hash += (hash << 10);
    hash ^= (hash >> 6);
    hash += (hash << 3);
    hash ^= (hash >> 11);
    hash += (hash << 15);
    return hash;
}

static __always_inline __u32 hash_payload(const __u8 *payload, __u32 length) {
    __u32 hash = 0;
    for (__u32 i = 0; i < length; i++) {
        hash += payload[i];
        hash += (hash << 10);
        hash ^= (hash >> 6);
    }
    hash += (hash << 3);
    hash ^= (hash >> 11);
    hash += (hash << 15);
    return hash;
}

static __always_inline void generate_random_payload(__u8 *payload, __u32 length) {
    for (__u32 i = 0; i < length; i++) {
        payload[i] = (__u8)(bpf_get_prandom_u32() % 256);
    }
}


SEC("socket_filter")
int tcp_packet_filter(struct __sk_buff *skb) {

    __u16 eth_proto;
    __u64 eth_proto_offset = offsetof(struct ethhdr, h_proto);
    if (bpf_skb_load_bytes(skb, eth_proto_offset, &eth_proto, sizeof(eth_proto)) < 0) {
        bpf_printk("Failed to load Ethernet protocol type");
        return 0;
    }

    eth_proto = bpf_ntohs(eth_proto);
    if (eth_proto != ETH_P_IP) {
        bpf_printk("Not an IP packet");
        return 0;
    }

    struct iphdr iph;
    __u64 ip_header_offset = sizeof(struct ethhdr);
    if (bpf_skb_load_bytes(skb, ip_header_offset, &iph, sizeof(iph)) < 0) {
        bpf_printk("Failed to load IP header");
        return 0;
    }

    if (iph.protocol == IPPROTO_TCP) {
        __u64 tcp_header_offset = ip_header_offset + iph.ihl * 4;

        struct tcphdr tcph;
        if (bpf_skb_load_bytes(skb, tcp_header_offset, &tcph, sizeof(tcph)) < 0) {
            bpf_printk("Failed to load TCP header");
            return 0;
        }

        __u8 payload[8];
        if (bpf_skb_load_bytes(skb, tcp_header_offset + sizeof(struct tcphdr), payload, sizeof(payload)) < 0) {
            bpf_printk("Failed to load TCP payload, generating random payload");
            generate_random_payload(payload, sizeof(payload));
        }

        __u32 sequence = bpf_ntohl(tcph.seq);  // Convert sequence number to host byte order
        bpf_printk("Captured TCP sequence number: %u", sequence);

        // struct packet_info packet_info = {
        //     .src_ip = bpf_ntohl(iph.saddr),
        //     .dst_ip = bpf_ntohl(iph.daddr),
        //     // .src_port = bpf_ntohs(tcph.source),
        //     // .dst_port = bpf_ntohs(tcph.dest),
        //     .payload_hash = hash_payload(payload, sizeof(payload)),
        //     .sequence = sequence,
        //     .protocol = iph.protocol,
        //     .length = skb->len, // Updated to total length including headers
        //     .time = bpf_ktime_get_ns(),
        // };

        struct packet_info packet_info = {
            .time = bpf_ktime_get_ns(),
        };

        __u32 index = hash_key(bpf_ntohl(iph.saddr), bpf_ntohl(iph.daddr), iph.protocol, hash_payload(payload, sizeof(payload)));
        bpf_map_update_elem(&tcplatency4, &index, &packet_info, BPF_ANY);

    } else if (iph.protocol == IPPROTO_UDP) {
        __u64 udp_header_offset = ip_header_offset + iph.ihl * 4;

        struct udphdr udph;
        if (bpf_skb_load_bytes(skb, udp_header_offset, &udph, sizeof(udph)) < 0) {
            bpf_printk("Failed to load UDP header");
            return 0;
        }

        __u8 payload[8];
        if (bpf_skb_load_bytes(skb, udp_header_offset + sizeof(struct udphdr), payload, sizeof(payload)) < 0) {
            bpf_printk("Failed to load UDP payload, generating random payload");
            generate_random_payload(payload, sizeof(payload));
        }

        // struct packet_info packet_info = {
        //     .src_ip = bpf_ntohl(iph.saddr),
        //     .dst_ip = bpf_ntohl(iph.daddr),
        //     // .src_port = bpf_ntohs(udph.source),
        //     // .dst_port = bpf_ntohs(udph.dest),
        //     .payload_hash = hash_payload(payload, sizeof(payload)),
        //     .sequence = 0,  // No sequence number for UDP
        //     .protocol = iph.protocol,
        //     .length = skb->len, // Updated to total length including headers
        //     .time = bpf_ktime_get_ns(),
        // };

        struct packet_info packet_info = {
            .time = bpf_ktime_get_ns(),
        };

        __u32 index = hash_key(bpf_ntohl(iph.saddr), bpf_ntohl(iph.daddr), iph.protocol, hash_payload(payload, sizeof(payload)));
        bpf_map_update_elem(&tcplatency4, &index, &packet_info, BPF_ANY);
    } else {
        bpf_printk("Unsupported IP protocol: %u", iph.protocol);
    }

    return 0;
}

char _license[] SEC("license") = "GPL";
