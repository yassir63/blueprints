import subprocess
import time

def run_scp_commands():
    commands = [
        # "iperf3 -u -t 5 -b 30M -c 192.168.3.2",
        # "iperf3 -t 5 -l 1490 -c 192.168.3.2",
        "../latency2/./tcplatency2_user",
        "../latency3/./tcplatency3_user",
        "scp ~/blueprints/ebpf/tcplatency_rttsampling/python/output2.txt root@fit02:~/blueprints/ebpf/tcplatency_rttsampling/python/",
        "scp ~/blueprints/ebpf/tcplatency_rttsampling/python/output3.txt root@fit02:~/blueprints/ebpf/tcplatency_rttsampling/python/"

    ]

    while True:
        for command in commands:
            try:
                result = subprocess.run(command, shell=True, check=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                print(f"Command '{command}' executed successfully.")
            except subprocess.CalledProcessError as e:
                print(f"Error executing command '{command}': {e.stderr.decode('utf-8')}")
        
        print("Waiting for 7 seconds before running the commands again...")
        time.sleep(7)

if __name__ == "__main__":
    run_scp_commands()
