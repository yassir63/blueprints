import os
import time
import shutil
import subprocess
import pandas as pd
from datetime import datetime

commands = [
    "chmod +x ../latency1/tcplatency1_user",
    "chmod +x ../latency4/tcplatency4_user",
    "../latency1/./tcplatency1_user",
    "../latency4/./tcplatency4_user",
    # "iperf3 -u -t 5 -b 30M -c 192.168.2.32"

]

def read_output(file_name):
    with open(file_name, 'r') as file:
        lines = file.readlines()
    data = {}
    for line in lines:
        parts = line.strip().split(",")
        key = int(parts[0].split(": ")[1])
        # sequence = int(parts[4].split(": ")[1])
        time_val = int(parts[1].split(": ")[1])
        if key not in data:
            data[key] = []
        data[key].append((key, time_val))
    return data

def find_closest_bigger_timestamp(output, target_time):
    closest_entry = None
    closest_time = float('inf')
    for key, entries in output.items():
        for _,time_val in entries:
            if time_val > target_time and time_val < closest_time:
                closest_time = time_val
                closest_entry = (key, time_val)
    return closest_entry

def calculate_delays(output1, output2, output3, output4):
    delays = []
    for key1, entries1 in output1.items():
        for _,time1 in entries1:
            if key1 in output2:
                for _,time2 in output2[key1]:
                    closest_entry3 = find_closest_bigger_timestamp(output3, time2)
                    if closest_entry3:
                        key3, time3 = closest_entry3
                        if key3 in output4:
                            for _,time4 in output4[key3]:
                                # if sequence4 == sequence3:
                                    final_delay = time4 - time1 - time3 + time2
                                    delays.append((time1, time2, time3, time4, final_delay))
    return delays

def main():
    # output_files = ['output1.txt', 'output2.txt', 'output3.txt', 'output4.txt']

    delay_data = []

    while True:

        output_files = ['output2.txt', 'output3.txt']

        all_files_exist = all(os.path.exists(file) for file in output_files)
        if all_files_exist:

            for command in commands:
                try:
                    result = subprocess.run(command, shell=True, check=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                    print(f"Command '{command}' executed successfully.")
                except subprocess.CalledProcessError as e:
                    print(f"Error executing command '{command}': {e.stderr.decode('utf-8')}")


            output1 = read_output('output1.txt')
            output2 = read_output('output2.txt')
            output3 = read_output('output3.txt')
            output4 = read_output('output4.txt')



        

            delays = calculate_delays(output1, output2, output3, output4)
            if delays:
                total_delay = 0
                for time1, time2, time3, time4, final_delay in delays:
                    print(f"Times: {time1}, {time2}, {time3}, {time4} -> Final delay: {final_delay}")
                    total_delay += final_delay
                average_delay = total_delay / len(delays)
                print(f"Average delay: {average_delay}")

                # Append the delay and current timestamp to the delay_data list
                delay_data.append({
                    'Timestamp': datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
                    'Delay': average_delay
                })

                # Check if we have 5 or more delay entries
                if len(delay_data) >= 10:
                    # Convert the delay_data list to a DataFrame
                    df = pd.DataFrame(delay_data)

                    # Generate a CSV file with the delays and timestamps
                    df.to_csv('delay_data.csv', index=False)
                    print("CSV file 'delay_data.csv' generated.")

                    # Exit the loop and end the program
                    return
            else:
                print("No matches found.")

            # Delete the output files
            for file in output_files:
                os.remove(file)
            
            # Wait 5 seconds before the next iteration
            time.sleep(5)
        else:
            print("Waiting for all output files to be available...")
            time.sleep(5)

if __name__ == "__main__":
    main()


# import os
# import time
# import subprocess
# import pandas as pd
# from datetime import datetime

# commands = [
#     "chmod +x ../latency1/tcplatency1_user",
#     "chmod +x ../latency4/tcplatency4_user",
#     "../latency1/./tcplatency1_user",
#     "../latency4/./tcplatency4_user"
# ]

# def read_output(file_name):
#     with open(file_name, 'r') as file:
#         lines = file.readlines()
#     data = {}
#     for line in lines:
#         parts = line.strip().split(",")
#         key = int(parts[0].split(": ")[1])
#         time_val = int(parts[1].split(": ")[1])
#         if key not in data:
#             data[key] = []
#         data[key].append(time_val)
#     return data

# def find_closest_bigger_timestamp(entries, target_time):
#     closest_time = float('inf')
#     for time_val in entries:
#         if time_val > target_time and time_val < closest_time:
#             closest_time = time_val
#     return closest_time

# def calculate_delays(output1, output2, output3, output4):
#     delays = []
#     print("Starting delay calculation...")
#     for key1, times1 in output1.items():
#         if key1 in output2:
#             for time1 in times1:
#                 for time2 in output2[key1]:
#                     print(f"Matching time1: {time1} with time2: {time2}")
#                     if key1 in output3:
#                         closest_time3 = find_closest_bigger_timestamp(output3[key1], time2)
#                         if closest_time3 != float('inf'):
#                             if key1 in output4:
#                                 for time4 in output4[key1]:
#                                     final_delay = time4 - time1 - closest_time3 + time2
#                                     print(f"Calculated final_delay: {final_delay} for times {time1}, {time2}, {closest_time3}, {time4}")
#                                     delays.append((time1, time2, closest_time3, time4, final_delay))
#                         else:
#                             print(f"No valid closest_time3 found for time2: {time2}")
#                     else:
#                         print(f"Key {key1} not found in output3")
#         else:
#             print(f"Key {key1} not found in output2")
#     return delays

# def main():
#     delay_data = []

#     while True:
#         output_files = ['output2.txt', 'output3.txt']

#         all_files_exist = all(os.path.exists(file) for file in output_files)
#         if all_files_exist:
#             for command in commands:
#                 try:
#                     result = subprocess.run(command, shell=True, check=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
#                     print(f"Command '{command}' executed successfully.")
#                 except subprocess.CalledProcessError as e:
#                     print(f"Error executing command '{command}': {e.stderr.decode('utf-8')}")

#             output1 = read_output('output1.txt')
#             output2 = read_output('output2.txt')
#             output3 = read_output('output3.txt')
#             output4 = read_output('output4.txt')

#             delays = calculate_delays(output1, output2, output3, output4)
#             if delays:
#                 total_delay = 0
#                 for time1, time2, time3, time4, final_delay in delays:
#                     print(f"Times: {time1}, {time2}, {time3}, {time4} -> Final delay: {final_delay}")
#                     total_delay += final_delay
#                 average_delay = total_delay / len(delays)
#                 print(f"Average delay: {average_delay}")

#                 # Append the delay and current timestamp to the delay_data list
#                 delay_data.append({
#                     'Timestamp': datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
#                     'Delay': average_delay
#                 })

#                 # Check if we have 40 or more delay entries
#                 if len(delay_data) >= 40:
#                     # Convert the delay_data list to a DataFrame
#                     df = pd.DataFrame(delay_data)

#                     # Generate a CSV file with the delays and timestamps
#                     df.to_csv('delay_data.csv', index=False)
#                     print("CSV file 'delay_data.csv' generated.")

#                     # Exit the loop and end the program
#                     return
#             else:
#                 print("No matches found.")

#             # Delete the output files
#             for file in output_files:
#                 os.remove(file)
            
#             # Wait 5 seconds before the next iteration
#             time.sleep(5)
#         else:
#             print("Waiting for all output files to be available...")
#             time.sleep(5)

# if __name__ == "__main__":
#     main()
