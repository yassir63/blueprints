import re
import time
import subprocess
import csv
from datetime import datetime


def parse_line(line):
    pattern = (r"Hash Key: (\d+), Source IP: ([\d.]+), Destination IP: ([\d.]+), "
               r"Protocol: (\d+), Sequence: (\d+), Length: (\d+), Time: (\d+)")
    match = re.match(pattern, line)
    if match:
        return {
            "hash_key": int(match.group(1)),
            "source_ip": match.group(2),
            "destination_ip": match.group(3),
            "protocol": int(match.group(4)),
            "sequence": int(match.group(5)),
            "length": int(match.group(6)),
            "time": int(match.group(7)),
            "original_line": line
        }
    return None


def sort_and_sum_lengths(input_file):
    with open(input_file, 'r') as file:
        lines = file.readlines()

    parsed_lines = [parse_line(line) for line in lines if parse_line(line) is not None]
    sorted_lines = sorted(parsed_lines, key=lambda x: x["time"])

    initial_timestamp = sorted_lines[0]["time"]
    target_timestamp = initial_timestamp + 1_000_000_000  # 1 second later

    print(initial_timestamp)
    print(target_timestamp)

    total_length = 0
    for entry in sorted_lines:
        if initial_timestamp <= entry["time"] <= target_timestamp:
            total_length += entry["length"]

    return total_length * 8 / 1000000


commands = [
    # "iperf3 -u -t 5 -c 192.168.2.1",
    # "iperf3 -u -b 20M -t 3 -c 192.168.2.1",
    # "iperf3 -n 30000 -l 1490 -c 192.168.2.31",
    "iperf3 -t 5 -c 192.168.2.31",

    # "iperf3 -t 2 -c 192.168.2.31",
    # "iperf3 -t 20 -c 192.168.2.31 --file ~/kali-linux-2023.4-vmware-amd64-s001.vmdk",
    "chmod +x tcplatency3_user",
    "./tcplatency3_user"
]

input_file = 'output3.txt'
results = []

while True:
    for command in commands:
        try:
            result = subprocess.run(command, shell=True, check=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            print(f"Command '{command}' executed successfully.")
        except subprocess.CalledProcessError as e:
            print(f"Error executing command '{command}': {e.stderr.decode('utf-8')}")

    total_length = sort_and_sum_lengths(input_file)
    timestamp = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    print(f"Total length of packets in the 1-second interval: {total_length} Mbps at {timestamp}")

    results.append({'timestamp': timestamp, 'total_length': total_length})

    if len(results) >= 50:
        with open('output_TCP50Inria.csv', 'w', newline='') as csvfile:
            fieldnames = ['timestamp', 'total_length']
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
            writer.writeheader()
            for result in results:
                writer.writerow(result)
        results.clear()  # Clear the results after writing to the file
        exit()

    time.sleep(5)
