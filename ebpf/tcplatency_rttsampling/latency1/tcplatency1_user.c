#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <linux/bpf.h>
#include <bpf/libbpf.h>
#include <errno.h>

#define MAP_PATH "/sys/fs/bpf/tc/globals/tcplatency1"
#define OUTPUT_FILE "output1.txt"

struct tcp_packet_info {
    // __u32 src_ip;
    // __u32 dst_ip;
    // __u16 protocol;
    // __u32 sequence;
    // __u32 length;
    // __u32 payload_hash;
    __u64 time;
} __attribute__((packed));

int main() {
    int map_fd, key = 0, next_key;
    FILE *output_file;

    // Open the BPF map
    map_fd = bpf_obj_get(MAP_PATH);
    if (map_fd < 0) {
        perror("Failed to open BPF map");
        return 1;
    }

    // Open the output file
    output_file = fopen(OUTPUT_FILE, "w");
    if (output_file == NULL) {
        perror("Failed to open output file");
        close(map_fd);
        return 1;
    }

    struct tcp_packet_info packet_info;

    while (1) {
        // Iterate through all keys
        if (bpf_map_get_next_key(map_fd, &key, &next_key) != 0) {
            // If no more keys, exit the loop
            if (errno == ENOENT) {
                break;
            } else {
                perror("bpf_map_get_next_key");
                break;
            }
        }

        // Look up the packet information from the BPF map
        if (bpf_map_lookup_elem(map_fd, &next_key, &packet_info) == 0) {
            // Convert IP addresses to human-readable format
            // char src_ip[INET_ADDRSTRLEN];
            // char dst_ip[INET_ADDRSTRLEN];
            // inet_ntop(AF_INET, &packet_info.src_ip, src_ip, INET_ADDRSTRLEN);
            // inet_ntop(AF_INET, &packet_info.dst_ip, dst_ip, INET_ADDRSTRLEN);

            // // Write the retrieved packet information to the output file
            // fprintf(output_file, "Hash Key: %u, Source IP: %s, Destination IP: %s, Protocol: %u, Sequence: %u, Length: %u, Time: %lld\n",
            //         next_key,
            //         src_ip,
            //         dst_ip,
            //         packet_info.protocol,
            //         packet_info.sequence,
            //         packet_info.length,
            //         packet_info.time);


            // Write the retrieved packet information to the output file
            fprintf(output_file, "Hash Key: %u,Time: %lld\n",
                    next_key,
                    packet_info.time);

            // Delete the element from the map
            if (bpf_map_delete_elem(map_fd, &next_key) != 0) {
                perror("Failed to delete BPF map element");
                break;
            }
        } else {
            perror("Failed to lookup BPF map element");
            break;
        }

        key = next_key; // Move to the next key
    }

    // Clean up
    fclose(output_file);
    close(map_fd);

    return 0;
}
